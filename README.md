# RainbowServer
## Opis
Część serwerowa projektu Rainbow, który zakłada stworzenie serwera na Raspberry Pi oraz klienta BLE GATT do sterowania diodą. Niestety - podczas tworzenia projektu nie byłem w stanie zainstalować zależności (https://github.com/PureSwift/BluetoothLinux) z uwagi na błąd kompilacji (naprawiony błąd kompilatora od 4.1.2), dlatego w projekcie znajduje się jedynie regulacja świecenia diodą.

## Część kliencka
Klient został napisany przez innego developera w iOS, dlatego nie ma do niego dostępu. Prawa do działającej wersji napisanej w języku Python także zostały przekazane.

## Instalacja
* Potrzeba instalacji prekompilowanego dla ARMv7 wersji Swift, np. z https://www.uraimo.com/2018/06/13/A-big-update-on-Swift-4-1-2-for-raspberry-pi-zero-1-2-3/
* Po zgraniu na RPi plików projektu należy wywołać komendę `swift run` w folderze projektu.

## Do poprawy
* Zaimplementowany PWM nie działa tak sprawnie jak jego gotowa wersja
* Wdrożenie stosu BLE
