//
//  main.swift
//  Rainbow
//
//  Created by Mikołaj Styś on 13.07.2018.
//  Copyright © 2018 Mikołaj Styś. All rights reserved.
//

import Foundation

//add package
// if linux

struct Color {
    let red: UInt8
    let green: UInt8
    let blue: UInt8
    
    static let black = Color(rgb: 0)
    static let white = Color(rgb: 0xFFFFFF)
    static let red = Color(r: 255, g: 0, b: 0)
    static let blue = Color(r: 0, g: 0, b: 255)
    static let green = Color(r: 0, g: 255, b: 0)

    init(r: UInt8, g: UInt8, b: UInt8) {
        red = r
        green = g
        blue = b
    }

    // Extracts channels from int
    init(rgb: UInt32) {
        red = UInt8((rgb >> 16) & 0xFF)
        green = UInt8((rgb >> 16) & 0xFF)
        blue = UInt8(rgb & 0xFF)
    }

    // Accepts color value in format "#RRGGBB" or "RRGGBB"
    init?(_ value: String) {
        var rgbString = value
        if value.starts(with: "#") {
            rgbString.removeFirst()
        }
        guard let rgb = UInt32(rgbString, radix: 16) else {
            return nil
        }
        self.init(rgb: rgb)
    }

}

protocol RgbDriver {
    
    var redChannel: UInt8 { get set }
    var greenChannel: UInt8 { get set }
    var blueChannel: UInt8 { get set }
    
    func start()
    func stop()
}

final class MockedRgbDriver: RgbDriver {
    
    var redChannel: UInt8 = 0
    var greenChannel: UInt8 = 0
    var blueChannel: UInt8 = 0
    
    func start() {
    }
    
    func stop() {
    }
}

final class SoftwarePwmRgbDriver: RgbDriver {
    
    private let pwmQueue = DispatchQueue(label: "pwmThread", qos: .background)
    private let interval: TimeInterval = 1 / (50 * 100)
    
    static let instance = SoftwarePwmRgbDriver()
    
    var redChannel: UInt8 = 0
    var greenChannel: UInt8 = 0
    var blueChannel: UInt8 = 0
    private var running: Bool = false
    
    private init() {
        
    }
    
    func start() {
        guard !running else { return }
        run()
    }
    
    func stop() {
        running = false
    }
    
    private func run() {
        running = true
        pwmQueue.async { [unowned self, interval] in
            while self.running {
                print("\(self.redChannel) - \(self.greenChannel) - \(self.blueChannel)")
                Thread.sleep(forTimeInterval: interval)
            }
        }
    }
}

final class Utils {
    
    static func computePwmFill(value: UInt8) -> UInt8 {
        return UInt8((Int(value) * 100) / 255)
    }
}

final class RgbController {
    
    static var driver: RgbDriver = MockedRgbDriver()
    
    static var color = Color.black {
        didSet {
            set(color: color)
        }
    }
    
    static func start() {
        driver.start()
    }
    
    static func stop() {
        driver.stop()
    }
    
    private static func set(color: Color) {
        driver.redChannel = Utils.computePwmFill(value: color.red)
        driver.blueChannel = Utils.computePwmFill(value: color.blue)
        driver.greenChannel = Utils.computePwmFill(value: color.green)
    }
    
    static func turnOff() {
        color = Color.black
    }
}


RgbController.color = Color.white
RgbController.start()
Thread.sleep(forTimeInterval: 2)
RgbController.color = Color.blue
Thread.sleep(forTimeInterval: 5)
RgbController.color = Color.green

RunLoop.main.run()
